﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateExercises
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> numbersLessThan50 = SelectIntegers(IntGeneratorService.GenerateListOfRandomInts(20), IsLessThan50);
            PrintListToConsole("Alle tal mindre end 50: ", numbersLessThan50);
            List<int> evenNumbers = SelectIntegers(IntGeneratorService.GenerateListOfRandomInts(20), IsEven);
            PrintListToConsole("Alle lige tal: ", evenNumbers);
            List<int> squareNumbers = SelectIntegers(IntGeneratorService.GenerateListOfRandomInts(50), IsSquareNumber);
            PrintListToConsole("Alle kvadrat tal: ", squareNumbers);
            Console.WriteLine("Done...");
            Console.Read();
        }


        private delegate bool myDelegate(int number);

        private static List<int> SelectIntegers(List<int> listOfIntegers, myDelegate del)
        {
            List<int> numbersSelected = new List<int>();
            foreach (var number in listOfIntegers)
            {
                if (del(number))
                {
                    numbersSelected.Add(number);
                }
            }
            return numbersSelected;

        }

        private static bool IsLessThan50(int number)
        {
            return number < 50;
        }

        private static bool IsEven(int number)
        {
            return number % 2 == 0;
        }

        private static bool IsSquareNumber(int number)
        {
            double sqrt = Math.Sqrt(number);
            return Math.Floor(sqrt) == sqrt;
        }

        private static void PrintListToConsole(string msg, List<int> listToPrint)
        {
            Console.Write(msg);
            Console.Write("[ ");
            foreach (int i in listToPrint)
            {
                Console.Write(i + " ,");
            }
            Console.WriteLine(" ]");
        }
    }
}

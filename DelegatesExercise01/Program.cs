﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DelegatesExercise.Model;

namespace DelegatesExercise
{
    class Program
    {
        static void Main(string[] args)
        {
            var people = GenerateTestData.CreateListOfPeople();
            while (true)
            {
                Console.WriteLine("----------------------------------------------------");
                Console.WriteLine("Hvordan vil du have udskrevet navnene?");
                Console.WriteLine("1: Efternavn efterfulgt af fornavn");
                Console.WriteLine("2: Efternavn efterfulgt af fornavn, store bogstaver");
                Console.WriteLine("3: Efternavn efterfulgt af fornavn, små bogstaver");
                Console.WriteLine("4: Kun forbogstav af fornavn efterfulgt af efternavn");
                Console.WriteLine();
                Console.WriteLine("x: For at afslutte!");
                Console.Write("> ");
                var input = Console.ReadLine();
                switch (input.ToUpper())
                {
                    case "1": people.ForEach(p => p.PrintPerson(FullNameLastNameFirst));
                        break;
                    case "2": people.ForEach(p => p.PrintPerson(FullNameAllCaps));
                        break;
                    case "3": people.ForEach(p => p.PrintPerson(FullNameLowerCase));
                        break;
                    case "4":
                        people.ForEach(p => p.PrintPerson(ShortName));
                        break;
                    case "X": return;
                    default: Console.WriteLine("Ukendt valg, prøv igen");
                        break;
                }
            }
        }

        private static string FullNameLastNameFirst(Person person)
        {
            return $"{person.LastName}, {person.FirstName}";
        }

        private static string FullNameAllCaps(Person person)
        {
            return $"{person.LastName.ToUpper()}, {person.FirstName.ToUpper()}";
        }

        private static string FullNameLowerCase(Person person)
        {
            return $"{person.LastName.ToLower()}, {person.FirstName.ToLower()}";
        }

        private static string ShortName(Person person)
        {
            return $"{person.FirstName.Substring(0, 1)}. {person.LastName}";
        }
    }


}

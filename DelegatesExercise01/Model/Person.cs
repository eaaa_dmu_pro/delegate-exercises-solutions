﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegatesExercise.Model
{
    public class Person
    {
        public delegate string PersonFormatter(Person person);

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public void PrintPerson(PersonFormatter formatter)
        {
            Console.WriteLine(formatter(this));
        }
    }
}
